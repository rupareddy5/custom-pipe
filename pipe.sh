#!/usr/bin/env bash
source "$(dirname "$0")/common.sh"

app_location=${app_location:?'app_location environment variable missing.'}
azure_static_web_apps_api_token=${azure_static_web_apps_api_token:?'azure_static_web_apps_api_token environment variable missing.'}


cd /bin/staticsites/
./StaticSitesClient 'upload'
